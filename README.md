# YARR_plot

Scripts for analyzing and plotting YARR scan data (and their comparisons)

## Environment Setting

The scripts in this repository requires some Python packages. We recommend using `conda` to set up the Python environment.
```
git clone https://gitlab.cern.ch/slac_itk/yarr_plot.git
cd yarr_plot
conda env create -f environment.yml
conda activate yarr_plot
```
