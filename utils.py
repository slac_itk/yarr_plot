import glob
import json
import os
import numpy as np

def load_scan_dirs(module_path, scan_type):
    scan_dirs = {}
    std_path = glob.glob(f"{module_path}/*std_{scan_type}*")
    if std_path:
        scan_dirs["std"] = sorted(std_path)[-1]
    else:
        for FE in ["syn", "lin", "diff"]:
            path = glob.glob(f"{module_path}/*{FE}_{scan_type}*")
            scan_dirs[FE] = sorted(path)[-1] if path else ""
    return scan_dirs

def parse_chips_from_scan(scan_path):
    with open(f"{scan_path}/scanLog.json", 'r') as f:
        scan_log_dict = json.load(f)
    return scan_log_dict["connectivity"][0]

def get_chip_name(chip_config_path, chip_type="RD53A"):
    with open(chip_config_path, 'r') as f:
        return json.load(f)[chip_type]["Parameter"]["Name"]

def match_chips_from_scans(scan_before_path, scan_after_path,
                           chip_type="RD53A"):
    chip_dict_before = parse_chips_from_scan(scan_before_path)
    chip_dict_after = parse_chips_from_scan(scan_after_path)

    if chip_dict_before["chipType"] != chip_dict_after["chipType"]:
        raise Exception("ERROR: The two scans have different chip type!\n" + \
                        f"Before: {chip_dict_before['chipType']}, After: {chip_dict_after['chipType']}")

    enabled_chips_before = [chip_config for chip_config in chip_dict_before["chips"] if chip_config["enable"]]
    enabled_rx_before = [chip_config['rx'] for chip_config in enabled_chips_before]
    enabled_chips_after = [chip_config for chip_config in chip_dict_after["chips"] if chip_config["enable"]]
    enabled_rx_after = [chip_config['rx'] for chip_config in enabled_chips_after]
    if enabled_rx_before == enabled_rx_after:
        enabled_rx = enabled_rx_before
        print(f"Comparing chips with `rx` {enabled_rx} from both scans! (Note: zero-base counting)")
    else:
        enabled_rx = [rx for rx in enabled_rx_before if rx in enabled_rx_after]
        print(f"Different `rx` found in before & after scans! Comparing chips with `rx` {enabled_rx} (Note: zero-base counting)")

    chip_name_dict = {}
    for rx in enabled_rx:
        config_before_name = os.path.basename(
            [chip_config["config"] for chip_config in enabled_chips_before if chip_config["rx"] == rx][0]
        )
        config_after_name = os.path.basename(
            [chip_config["config"] for chip_config in enabled_chips_after if chip_config["rx"] == rx][0]
        )
        # if config_before_name != config_after_name:
        #     raise Exception("ERROR: Different configs used in two scans! Cannot compare!\n" + \
        #                     f"Chip: {i+1}, config before: {config_before_name}, config after: {config_after_name}")
        chip_name_dict[f"Chip{rx+1}"] = get_chip_name(f"{scan_before_path}/{config_before_name}.before", chip_type)

    return chip_name_dict


def load_scan_data(scan_dirs, chip_id, data_type="OccupancyMap"):
    def subroutine(scan_path, chip_id, data_type):
        data_path = f"{scan_path}/{chip_id}_{data_type}.json"
        if os.path.exists(data_path):
            with open(data_path, 'r') as f:
                scan_data = np.array(json.load(f)["Data"])
            return scan_data
        else:
            return np.zeros((400, 192))

    if "std" in scan_dirs.keys():
        return subroutine(scan_dirs["std"], chip_id, data_type).T
    else:
        if scan_dirs["syn"]:
            syn_data = subroutine(scan_dirs["syn"], chip_id, data_type).T[:, :128]
        else:
            syn_data = np.zeros((192, 128))
        if scan_dirs["lin"]:
            lin_data = subroutine(scan_dirs["lin"], chip_id, data_type).T[:, 128:264]
        else:
            lin_data = np.zeros((192, 136))
        if scan_dirs["diff"]:
            diff_data = subroutine(scan_dirs["diff"], chip_id, data_type).T[:, 264:]
        else:
            diff_data = np.zeros((192, 136))
        return np.hstack([syn_data, lin_data, diff_data])
