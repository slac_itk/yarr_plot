import numpy as np

from utils import *

import matplotlib.pyplot as plt
import atlas_mpl_style as ampl
ampl.use_atlas_style(atlasLabel='ITk Pixels')
ampl.set_color_cycle("MPL", n=10)
default_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

class ComparisonPlot:
    def __init__(self,
                 path_before, path_after,
                 module_desc=""
                ):
        self.path_before = path_before
        self.path_after = path_after
        self.module_desc = module_desc
        # self.before_scan_data = parse_scan_data(before_scan_dirs, chip_id, data_type)
        # self.after_scan_data = parse_scan_data(after_scan_dirs, chip_id, data_type)
        # self.system_name = system_name
        # self.module_name = module_name
        # self.chip_name, self.chip_id = chip_name, chip_id
        self.chips = {}
        self.data = {}

    def load_data(self, scan_type, data_type="OccupancyMap", chip_type="RD53A"):
        before_scan_dirs = load_scan_dirs(self.path_before, scan_type)
        print(before_scan_dirs)
        after_scan_dirs = load_scan_dirs(self.path_after, scan_type)
        print(after_scan_dirs)

        self.chips = match_chips_from_scans(list(before_scan_dirs.values())[0],
                                            list(after_scan_dirs.values())[0],
                                            chip_type)

        self.data = {}
        for chip_name, chip_id in self.chips.items():
            self.data[chip_name] = {
                "DataBefore": load_scan_data(before_scan_dirs, chip_id, data_type),
                "DataAfter": load_scan_data(after_scan_dirs, chip_id, data_type),
            }


    def hist2D_xor(self, target_val, desc_str, save_path=""):
        for chip_name, chip_id in self.chips.items():
            fig = plt.figure(figsize=(12, 6), dpi=200)
            ax = fig.add_subplot(111)

            ax.imshow(np.logical_xor(self.data[chip_name]["DataBefore"] == target_val,
                                     self.data[chip_name]["DataAfter"] == target_val),
                      cmap="Greys",
                      origin='lower')

            ampl.set_xlabel(r"Column")
            ampl.set_ylabel(r"Row")
            ampl.draw_atlas_label(0.02, 0.95,
                                  status='int',
                                  desc=f"{self.module_desc}\n{chip_name} ({chip_id})\n{desc_str}",
                                  size=16)
            if save_path:
                plt.savefig(save_path % chip_id, bbox_inches='tight')
            plt.show()


    def hist2D_diff(self, desc_str, vmin=-100, vmax=100, save_path=""):
        for chip_name, chip_id in self.chips.items():
            fig = plt.figure(figsize=(12, 6), dpi=200)
            ax = fig.add_subplot(111)

            im = ax.imshow(self.data[chip_name]["DataAfter"] - self.data[chip_name]["DataBefore"],
                           cmap="bwr",
                           vmin=vmin, vmax=vmax,
                           origin='lower')
            plt.colorbar(im)

            ampl.set_xlabel(r"Column")
            ampl.set_ylabel(r"Row")
            ampl.draw_atlas_label(0.02, 0.95,
                                  status='int',
                                  desc=f"{self.module_desc}\n{chip_name} ({chip_id})\n{desc_str}",
                                  size=16)
            if save_path:
                plt.savefig(save_path % chip_id, bbox_inches='tight')
            plt.show()

    def hist1D_all(self, desc_str, bins, save_path="",
                   xlabel=r"Pixel Values", ylabel=r"# of Pixels / bin"):
        for chip_name, chip_id in self.chips.items():
            fig = plt.figure(figsize=(12, 6), dpi=200)
            ax = fig.add_subplot(111)

            ax.hist(self.data[chip_name]["DataAfter"].flatten(), bins=bins,
                    label="After", histtype='bar', alpha=0.4)

            ax.hist(self.data[chip_name]["DataBefore"].flatten(), bins=bins,
                    label="Before", histtype='step', linewidth=1.5, color=default_colors[0])

            ampl.set_xlabel(xlabel)
            ampl.set_ylabel(ylabel)
            ampl.draw_atlas_label(0.02, 0.95,
                                  status='int',
                                  desc=f"{self.module_desc}\n{chip_name} ({chip_id})\n{desc_str}",
                                  size=16)
            ax.legend(loc='upper right')
            if save_path:
                plt.savefig(save_path % chip_id, bbox_inches='tight')
            plt.show()

    def hist1D_FE(self, desc_str, bins, save_path="",
                  xlabel=r"Pixel Values", ylabel=r"# of Pixels / bin"):
        for chip_name, chip_id in self.chips.items():
            fig = plt.figure(figsize=(12, 6), dpi=200)
            ax = fig.add_subplot(111)

            ax.hist(self.data[chip_name]["DataAfter"][:, :128].flatten(), bins=bins,
                    label="After, Syn FE", histtype='bar', alpha=0.4)
            ax.hist(self.data[chip_name]["DataAfter"][:, 128:264].flatten(), bins=bins,
                    label="After, Lin FE", histtype='bar', alpha=0.4)
            ax.hist(self.data[chip_name]["DataAfter"][:, 264:].flatten(), bins=bins,
                    label="After, Diff FE", histtype='bar', alpha=0.4)

            ax.hist(self.data[chip_name]["DataBefore"][:, :128].flatten(), bins=bins,
                    label="Before, Syn FE", histtype='step', linewidth=1.5, color=default_colors[0])
            ax.hist(self.data[chip_name]["DataBefore"][:, 128:264].flatten(), bins=bins,
                    label="Before, Lin FE", histtype='step', linewidth=1.5, color=default_colors[1])
            ax.hist(self.data[chip_name]["DataBefore"][:, 264:].flatten(), bins=bins,
                    label="Before, Diff FE", histtype='step', linewidth=1.5, color=default_colors[2])

            ampl.set_xlabel(xlabel)
            ampl.set_ylabel(ylabel)
            ampl.draw_atlas_label(0.02, 0.95,
                                  status='int',
                                  desc=f"{self.module_desc}\n{chip_name} ({chip_id})\n{desc_str}",
                                  size=16)
            ax.legend(loc='upper right')
            if save_path:
                plt.savefig(save_path % chip_id, bbox_inches='tight')
            plt.show()
